from datetime import datetime
import json
import requests
import os.path


responseTodos = requests.get("https://jsonplaceholder.typicode.com/todos")
responseUsers = requests.get("https://jsonplaceholder.typicode.com/users")
todos = json.loads(responseTodos.text)
users = json.loads(responseUsers.text)


def prepare_data(user):
    dataforfile = ""
    dataforfile += user["name"] + " <" + user["email"] + "> " + \
                   datetime.strftime(datetime.now(), "%d.%m.%Y %H:%M") + "\n"
    dataforfile += user["company"]["name"] + "\n\n"
    dataforfile += "Завершенные задачи:\n"
    for todo in todos:
        if todo["completed"] and todo["userId"] == user["id"]:
            temp = todo["title"]
            if len(temp) > 50:
                dataforfile += temp[:50] + "...\n"
            else:
                dataforfile += temp + "\n"
    dataforfile += "\nОставшиеся задачи:\n"
    for todo in todos:
        if not todo["completed"] and todo["userId"] == user["id"]:
            temp = todo["title"]
            if len(temp) > 50:
                dataforfile += temp[:50] + "...\n"
            else:
                dataforfile += temp + "\n"
    return dataforfile


def write_data_to_disk(data):
    if os.path.isfile("tasks/" + user["username"] + ".txt"):
        file_creation_time = datetime.strftime(
            datetime.fromtimestamp(os.path.getmtime(
                "tasks/" + user["username"] + ".txt")), "%Y-%m-%dT%H:%M")
        os.rename("tasks/" + user["username"] + ".txt", "tasks/" +
                  user["username"] + "_" + file_creation_time + ".txt")
        file = open("tasks/" + user["username"] + ".txt", "w")
        if file.write(data) == len(data):
            print("writing successful")
            file.close()
        else:
            print("writing failed")
            file.close()
            os.remove("tasks/" + user["username"] + ".txt")
            os.rename("tasks/" + user["username"] + "_" + file_creation_time +
                      ".txt", "tasks/" + user["username"] + ".txt")
    else:
        file = open("tasks/" + user["username"] + ".txt", "w")
        if file.write(data) == len(data):
            print("write successful")
            file.close()
        else:
            print("write not successful")
            file.close()
            os.remove("tasks/" + user["username"] + ".txt")
    return 0


if not os.path.exists("tasks"):
    os.mkdir("tasks")
if os.path.exists("tasks"):
    for user in users:
        write_data_to_disk(prepare_data(user))
